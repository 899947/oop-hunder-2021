﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       int zuZahlenderBetrag; 
       int rückgabebetrag;
       
       while(true) {
       System.out.printf("Fahrkartenbestellvorgang: %n%n");
       System.out.println("=========================");
       zuZahlenderBetrag=fahrkartenbestellungErfassen();
       rückgabebetrag=fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben();
       rueckgeldausgeben(rückgabebetrag);
       }
       
    }
    
    public static int fahrkartenbestellungErfassen() {
    	int zuZahlenderBetrag=0;
    	int anzahl=0;
    	int[] fahrkartenArt  = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};   			   
    	int[] fahrkartenPreis = {290, 330, 360, 190, 860,900,960,2350,2430,2490}; 
    	Scanner tastatur = new Scanner(System.in);
        
    	System.out.printf("Wählen Sie bitte Ihre Wunschfahrkarte aus: %n%n");
    	System.out.printf("Auswahlnummer    Bezeichnung                           Preis in Euro \n");
    	System.out.printf( "%-15d  %-26s  %10s[2,90  EUR] \n", 1,"Einzelfahrschein Berlin AB", "");
    	System.out.printf( "%-15d  %-26s  %10s[3,30  EUR] \n",2,"Einzelfahrschein Berlin BC","");
    	System.out.printf( "%-15d  %-26s  %10s[3,60  EUR] \n",3,"Einzelfahrschein Berlin AB","");
    	System.out.printf( "%-15d  %-26s  %10s[1,90  EUR] \n",4,"Kurzstrecke","");
    	System.out.printf( "%-15d  %-26s  %10s[8,60  EUR] \n",5,"Tageskarte Berlin AB","");
    	System.out.printf( "%-15d  %-26s  %10s[9,00  EUR] \n",6, "Tageskarte Berlin BC","");
    	System.out.printf( "%-15d  %-26s  %10s[9,60  EUR] \n",7,"Tageskarte Berlin ABC","");
    	System.out.printf( "%-15d  %-26s  %3s[23,50 EUR] \n",8,"Kleingruppen-Tageskarte Berlin AB","");
    	System.out.printf( "%-15d  %-26s  %3s[24,30 EUR] \n",9,"Kleingruppen-Tageskarte Berlin BC","");
    	System.out.printf( "%-15d  %-26s  %2s[24,90 EUR] \n\n",10,"Kleingruppen-Tageskarte Berlin ABC","");

		System.out.print("Ihre Wahl: ");
		int auswahl = tastatur.nextInt();
		   
		while(auswahl > fahrkartenArt.length || auswahl < 1) {			
			System.out.print(" >>falsche Eingabe<<%nIhre Wahl:");	
			System.out.print("Ihre Wahl: ");				
			auswahl = tastatur.nextInt();
		}	
		   
		for(int i = 0; i < fahrkartenArt.length; i++) {
			if(auswahl == fahrkartenArt[i]) 
			zuZahlenderBetrag = fahrkartenPreis[i]; 	      
			}
		
    	do{
    		
	        System.out.print("Anzahl der Tickets (zwischen 1 und 10): ");
	        anzahl = tastatur.nextInt();
	        
	        if (anzahl<1 || anzahl>10) {
	        	System.out.println("<<fehlerhafte Eingabe, Eingabe wiederholen>>");
	        }
    	}while(!(anzahl>=1 && anzahl<=10));    
        zuZahlenderBetrag=zuZahlenderBetrag*anzahl;
        return zuZahlenderBetrag;
        
    }
    
    public static int fahrkartenBezahlen (int zuZahlen) {
    	int eingezahlterGesamtbetrag = 0;
    	int eingeworfeneMünze=0;

    	Scanner tastatur = new Scanner(System.in);
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %d Cent %n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 200 Cent): ");
     	   eingeworfeneMünze = tastatur.nextInt();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
           
        }
        int rückgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
        return rückgabebetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         }
         System.out.println("\n\n");
    }
    
    public static void rueckgeldausgeben(int rückgabebetrag) {
    	if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " Cent");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 200) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 200;
            }
            while(rückgabebetrag >= 100) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 100;
            }
            while(rückgabebetrag >= 50) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 50;
            }
            while(rückgabebetrag >= 20) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 20;
            }
            while(rückgabebetrag >= 10) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 10;
            }
            while(rückgabebetrag >= 5)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 5;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n\n");
    }
}