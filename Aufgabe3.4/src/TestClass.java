
public class TestClass {

	public static void main(String[] args) {
 		Datum d1 = new Datum(1,1,2000);
		Datum d2 = new Datum(2,2,2021);
		
		Person p1 = new Person("Maxi", d1, 1);
		Person p2 = new Person("Hubi", d2, 2);
		

		
		System.out.printf("Name: "+p1.getName()+"   ");
		System.out.printf("Geburtsdatum: %02d.%02d.%d   Nummer: %d\n", d1.getTag(),d1.getMonat(), d1.getJahr(), p1.getNummer());
		System.out.printf("Name: "+p2.getName()+"   ");
		System.out.printf("Geburtsdatum: %02d.%02d.%d   Nummer: %d\n", d2.getTag(),d2.getMonat(), d2.getJahr(), p2.getNummer());
		System.out.println("Anzahl Personen: " + Person.getAnzahl());
	}
}
