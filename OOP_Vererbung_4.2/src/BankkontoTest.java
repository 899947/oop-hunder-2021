
public class BankkontoTest {

	public static void main(String[] args) {
	//Test Kontoerstellung	
		Bankkonto k1= new Bankkonto("Nico Hunder");// TODO Auto-generated method stub
		System.out.println(k1);
		
		Bankkonto k2= new Bankkonto("Nico Hunder");// TODO Auto-generated method stub
		System.out.println(k2);
		
	//Test einzahlen
		k1.einzahlen(50.5);
		System.out.println(k1);
		
	//Test auszahlen
		double auszahlungK1=k1.auszahlen(20);
		System.out.println(" "+auszahlungK1+" wurden ausgezahlt");
		System.out.println(k1);
		
	//Test Dispokonto
		Dispokonto dk1= new Dispokonto ("Fred Feuerstein", 200);
		System.out.println(dk1);
		double auszahlungDk1=dk1.auszahlen(300);
		System.out.println(" "+auszahlungDk1+" wurden ausgezahlt");
		System.out.println(dk1);
		
	//Test mit Array	
		Bankkonto[] konten= new Bankkonto[]{ (new Bankkonto("Alex")), new Bankkonto("Bernd"), new Dispokonto("Clara",100), new Dispokonto("Dustin", 300)};
		for(int i=0; i<konten.length ; i++)
		System.out.println(konten[i]);
	}
	
}
