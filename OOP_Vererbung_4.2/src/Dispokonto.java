
public class Dispokonto extends Bankkonto{
	private double dispokredit;
	
	public Dispokonto (String inhaber, double dispokredit){
		super(inhaber);
		setDispokredit(dispokredit);
		
	}

	public double getDispokredit() {
		return dispokredit;
	}

	public void setDispokredit(double dispokredit) {
		this.dispokredit = dispokredit;
	}
	
	public double auszahlen(double auszahlBetrag) {
		double ausgezahlterBetrag=0;
		if((this.kstand-auszahlBetrag)<=-this.dispokredit) {
			
			ausgezahlterBetrag=this.kstand+this.dispokredit;
			this.kstand=-this.dispokredit;
			return ausgezahlterBetrag;
		
		}
		
		else {
			this.kstand-=auszahlBetrag;
			return auszahlBetrag;
		}
			
	}	
	
	
	@Override
	public String toString() {   
		return super.toString() +" Dispoh�he: " + this.dispokredit;
	}
	
}
