
public class Bankkonto {
	private String inhaber;
	private int knummer;
	protected double kstand;
	private static int kontozaehler=0;
	
	//Konstruktor
	public Bankkonto (String inhaber){
		this.inhaber=inhaber;
		this.knummer=kontozaehler;
		this.kstand=0;
		kontozaehler++;
		
	}
	
	
	
	//Methoden
	public String getInhaber() {
		return inhaber;
	}
	public void setInhaber(String inhaber) {
		this.inhaber = inhaber;
	}
	public int getKnummer() {
		return knummer;
	}

	public double getKstand() {
		return kstand;
	}
	
	public void einzahlen(double einzahlBetrag) {
		if(einzahlBetrag>=0) {
			this.kstand+=einzahlBetrag;
		}
	}
	
	public double auszahlen(double auszahlBetrag) {
		if(auszahlBetrag>=0) {
			this.kstand-=auszahlBetrag;
			return auszahlBetrag;
		}
		
		else
			return this.kstand;
	}	
	@Override
	public String toString() {   //  [ Name: Max , Alter: 18 ]
		return " Inhaber: " + this.inhaber + " , Kontonummer: " + this.knummer + " ,Kontostand: " + this.kstand;
	}
	
	
}
