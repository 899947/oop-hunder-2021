
public class Kreis extends GeoObject {
	private double radius;
	
	public Kreis (double x, double y, double radius) {
		super(x,y);
		setRadius(radius);
		
	}
	
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	
	
	public double determineArea () {
		return this.radius*this.radius*3.1415;
	}
	
	public String toString() {
		return "Radius : " + this.radius;
		
	}
}
