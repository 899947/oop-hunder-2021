
public class Rechteck extends GeoObject {
	private double hoehe;
	private double breite;
	
	public Rechteck(double x, double y, double hoehe, double breite) {
		super(x,y);
		setHoehe(hoehe);
		setBreite(breite);
		
	}

	public double getHoehe() {
		return hoehe;
	}

	public void setHoehe(double hoehe) {
		this.hoehe = hoehe;
	}

	public double getBreite() {
		return breite;
	}

	public void setBreite(double breite) {
		this.breite = breite;
	}
	
	public double determineArea () {
		return hoehe*breite;
	}
	
	public String toString() {
		return "Hoehe : " + this.hoehe +" "+ "Breite : "+ this.breite ;
		
	}
}
