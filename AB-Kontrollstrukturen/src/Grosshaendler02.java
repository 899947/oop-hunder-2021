import java.util.Scanner;

public class Grosshaendler02 {

	public static void main(String[] args) {
		Scanner myScanner= new Scanner (System.in);
		
	
		System.out.println("Geben sie den Bestellwert ein (noch ohne MwST.):");
		
		int bestellwert = myScanner.nextInt();
		float bestellwertNeu;
		
		if(bestellwert>0 && bestellwert<100) {
			bestellwertNeu=(float) (bestellwert*0.9*1.19);
		}
		if(bestellwert>100 && bestellwert<500){
			bestellwertNeu=(float) (bestellwert*0.85*1.19);
		}
		else {
			bestellwertNeu=(float) (bestellwert*0.8*1.19);
		}
		System.out.printf("Der erm��igte Bestellwert ist %.2f Euro.", bestellwertNeu);

	}

}
