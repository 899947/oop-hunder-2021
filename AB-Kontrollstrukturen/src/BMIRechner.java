import java.util.Scanner;

public class BMIRechner {

	public static void main(String[] args) {
		Scanner myScanner= new Scanner (System.in);
		float bmi;
		
		System.out.println("Geben sie ihre Körpergröße[cm] an:");
		float groesse = myScanner.nextFloat();
		
		System.out.println("Geben sie ihr Körpergewicht[kg] an:");
		float gewicht = myScanner.nextFloat();
		
		System.out.println("Geben sie ihr Geschlecht an [m/w]:");
		char geschlecht = myScanner.next().charAt(0);
		
		bmi=gewicht/((groesse/100)*(groesse/100));
		
		if(geschlecht=='m' ) {
			if(bmi<20) {
				System.out.printf("Klassifikation: Untergewicht");
			}
			else if(bmi>20 && bmi<25) {
				System.out.printf("Klassifikation: Normalgewicht");
			}
			else {
				System.out.printf("Klassifikation: Übergewicht");
			}
		}	
		else if(geschlecht=='w' ) {
			if(bmi<19) {
				System.out.printf("Klassifikation: Untergewicht");
			}
			else if(bmi>19 && bmi<24) {
				System.out.printf("Klassifikation: Normalgewicht");
			}
			else {
				System.out.printf("Klassifikation: Übergewicht");
			}
		}		
	}

}
