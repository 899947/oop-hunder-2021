import java.util.Scanner;

public class Grosshaendler01 {

	public static void main(String[] args) {
		Scanner myScanner= new Scanner (System.in);
		
		System.out.println("Preis pro Maus: 10 Euro");
		System.out.println("Wieviele M�use wollen sie bestellen?");
		
		int anzahl = myScanner.nextInt();
		float rechnungsbetrag;
		
		if(anzahl>9) {
			rechnungsbetrag=(float) (anzahl*10*1.19);
		}
		else {
			rechnungsbetrag=(float) (anzahl*10*1.19+10);
		}
		
		System.out.printf("Der Rechnungsbetrag ist %.2f Euro.", rechnungsbetrag);
	}

}
