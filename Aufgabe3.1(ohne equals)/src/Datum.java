public class Datum {
	
	private int tag;
	private int monat;
	private int jahr;
	
	
	public Datum() {
		this.tag = 1;
		this.monat = 1;
		this.jahr = 1970;
	}
	
	public Datum( int tag, int monat, int jahr) {
		setTag(tag);
		setMonat(monat);
		setJahr(jahr);
	}
	
	public int getTag() {
		return tag;
	}
	public void setTag(int tag) {
		if(tag <= 31 && tag > 0)
			this.tag = tag;
	}
	
	public int getMonat() {
		return monat;
	}
	public void setMonat(int monat) {
		if(monat <= 12 && monat > 0)
			this.monat = monat;
	}
	
	public int getJahr() {
		return jahr;
	}
	public void setJahr(int jahr) {
		this.jahr = jahr;
	}
	public String toString() {
		return this.tag + "." + this.monat + "." + this.jahr;
	}
	
	public boolean equals(Object obj) {
				
		if(obj instanceof Datum) {
			Datum d = (Datum) obj;
			if(this.tag == d.getTag() && this.monat == d.getMonat() && this.jahr == d.getJahr()) { 
				return true;
		}
		else
			return false;
		}
		return false;
		}
	

		 }
