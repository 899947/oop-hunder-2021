
public class KomplexeZahl {
	
	// Attribute
	private double realteil;
	private double imteil;
	
	// Konstuktoren
	public KomplexeZahl() {
		this.realteil = 0.0;
		this.imteil   = 0.0;
	}
	
	public KomplexeZahl(double realteil, double imteil) {
		setRealteil(realteil);
		setImteil(imteil);
	}
	
	// Methoden
	public double getRealteil() {
		return this.realteil;
	}
	public double getImteil() {
		return this.imteil;
	}
	
	public void setRealteil(double realteil) {
		this.realteil = realteil;
	}
	
	public void setImteil(double imteil) {
		this.imteil = imteil;
	}
	
	public KomplexeZahl multKomplex2(KomplexeZahl z) {
		double real, ima;
		real = this.realteil*z.getRealteil()  - this.imteil*z.getImteil();
		ima =  this.imteil*z.getRealteil() + this.realteil*z.getImteil();
		return new KomplexeZahl(real, ima);
	}
	
	// Klassenmethode
	public static KomplexeZahl multKomplex(KomplexeZahl z1, KomplexeZahl z2) {
		double a, b;
		a = z1.getRealteil() * z2.getRealteil() - z1.getImteil()*z2.getImteil();
		b = z1.getRealteil() * z2.getImteil() + z2.getRealteil()*z1.getImteil();
		return new KomplexeZahl(a, b);
	}
	
	
	public String toString() {
		return String.format("%.1f%+.1fj %n", this.realteil,  this.imteil);
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof KomplexeZahl) {
			KomplexeZahl k = (KomplexeZahl) obj;
			if(this.realteil == k.getRealteil() && this.imteil == k.getImteil()) {
				return true;
			}
			else 
				return false;
		}
		return false;
	}
	
}
