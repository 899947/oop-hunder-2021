
public class Main {

	public static void main(String[] args) {
		KomplexeZahl c1 = new KomplexeZahl();
		KomplexeZahl c2 = new KomplexeZahl();
		c2.setRealteil(4);
		c2.setImteil(3);
		KomplexeZahl c3 = new KomplexeZahl(6, -1);
				
		System.out.print(c1);
		System.out.print(c2);
				
		if(c1.equals(c2))
			System.out.println("Real- und Imaginärteil sind gleich");
		else
			System.out.println("Real- und Imaginärteil sind ungleich");
				
		KomplexeZahl c4 = KomplexeZahl.multKomplex(c2, c3);
		KomplexeZahl c5 = c2.multKomplex2(c3);
				
		System.out.print(c4);
		System.out.print(c5);

	}

}
