import java.util.Scanner;

public class Aufgabe3 {

public static void main(String[] args) {
Scanner eingabe = new Scanner(System.in);
System.out.println("Geben sie bitte eine natürliche Zahl ein: ");
int querZahl = eingabe.nextInt();

int quersumme = 0;
int zehnerpotenz = 10;
System.out.printf("Die Quersumme von %d ist ", querZahl);
do {
quersumme += querZahl % zehnerpotenz;
querZahl /= zehnerpotenz;
} while (Math.abs(querZahl) >= 1);
System.out.printf("%d.%n", quersumme);
}

}