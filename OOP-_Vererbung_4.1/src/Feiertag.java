
public class Feiertag extends Datum {
	private String feiertag;
	
	public Feiertag() {
		super();
		setFeiertag("...");
		
	}
	
	public Feiertag (int tag, int monat, int jahr, String feiertag ) {
		super(tag, monat, jahr);
		setFeiertag(feiertag);
		
	}

	public String getFeiertag() {
		return feiertag;
	}

	public void setFeiertag(String feiertag) {
		this.feiertag = feiertag;
	}
	
	@Override
	public String toString(){
		//return "Name:" + getName() + " , Alter: " + getAlter() +" ,Status:"+ this.status;
		return super.toString() +" (" +this.feiertag +")";
	}
	
}
