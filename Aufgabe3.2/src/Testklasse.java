public class Testklasse {

	public static void main(String[] args) {
		 
		
		Datum d1 = new Datum();
		Datum d2 = new Datum();
		d2.setTag(5);
		d2.setMonat(6);
		d2.setJahr(2011);
		Datum d3 = new Datum(1, 1, 2021);
		Datum d4 = new Datum(1, 1, 2021);
		
		System.out.printf("%02d.%02d.", d1.getTag(),d1.getMonat());
		System.out.println(d1.getJahr());
		
		System.out.printf("%02d.%02d.", d2.getTag(),d2.getMonat());
		System.out.println(d1.getJahr());
		
		System.out.printf("%02d.%02d.", d3.getTag(),d3.getMonat());
		System.out.println(d1.getJahr());
		
		System.out.println(d4); // System.out.println(d4.toString());

		if(d3.equals(d4)) 
			System.out.println("Datum gleich");
		else
			System.out.println("Datum ungleich");
		System.out.println("Das Quartal von "+ d2 + " ist " + Datum.berechneQuartal(d2));
	}

}